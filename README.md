# webhelp-bot #

## funzioni base ##
---
le funzioni base sono attivi sempre e per chiunque decide di aggiungere il bot
* ban nickname con caratteri cirillici (e aggiunta al database bot) #feature:nick_ban
* ban nickname con caratteri arabi (e aggiunta al database bot) #feature:nick_ban
* segnalazione (o ban automatico) per bot/spammers nel db #feature:nick\_ban\_log
* eliminazione messaggio di join di telegram #feature:nick\_ban\_delete
* ban nickname con parole relative sesso (sex|fuck) #feature:sex\_in\_nick
* ban se presente nella lista CAS di @combot
* eliminazione messaggio con link di utenti appena entrati #feature:early\_join
    * time frame: 3600 (default)
* `DRAFT`: come early\_join ma con ban aggiuntivo #feature:early\_join\_ban
* se utente non ha username viene kickato #feature:no\_nick\_kick
* se utente non ha username viene bannato #feature:no\_nick\_ban

## funzioni avanzate ##
---
le funzioni avanzate richiedono il setup del canale, ovvero moderatori
* saluto di welcome personalizzato
* saluto di goodbye personalizzato
* saluto, personalizzato
* `/info` torna la lista dello staff ufficiale del canale, inclusi i moderatori
* `/admin` torna la lista (in privato) dello staff ufficiale
* `/warn` richiama l'attenzione di tutti i moderatori

### funzioni da moderatore ###
---
queste funzioni sono disponibili solo per i moderatori
* `/mute` muta un utente
* `/unmute` un-muta un utente
* `/kick` banna un utente in maniera permanente
* `/delete` cancella il messaggio in reply
* `/kickdelete` cancella il messaggio in reply e kicka utente in maniera permanente
* `/unban` toglie il ban a un utente
* `/spam` questa funzione aggiunge un utente alla lista shared degli spammers
* inviare messaggi/annunci tramite bot
* `/stats` statistiche avanzate sul numero utenti presenti day by day

## funzioni da attivare ##
---
richiedono ulteriore personalizzazione
* ammonizioni, con eventuale ban al cap
* search in vari motori di ricerca
* ticker di cryptovalute
* inviti
* punti/coin/stake
* `/wallet` interno e funzioni da admin per incrementare il wallet
  * `/up UID COIN` for topup
* gioco guess price `!guess`
  * `/guess n`    | register your guess price
  * `/guessl`     | list of guess price
  * `/guessw n`   | (admin) select winner
  * `/guessc`     | (admin) reset
  * `/guessp`     | (admin) give prizes
  * `/guessd n`   | (admin) clear guess n
  * `/startguess` | (admin) start
  * `/stopguess`  | (admin) stop
* cleaning di stickers,gif,eth address (per ICO) altro
* mercatino interno

## features importanti ##
* il bot ha delle features, queste possono essere abilitate e disabilitate a piacere, in base al gusto o richieste del singolo canale
* il bot permette la localizzazione dei messaggi (vedi sotto)
* il bot permette la personalizzazione dei testi, ogni canale puo avere i suoi testi personalizzabili
```
$translations[$groups['SAFER']] = array(
  'welcome' => 'Ciao! Benvenuto nel gruppo Telegram di S4fer',
  'goodbye' => 'Arrivederci, stammi bene!',
  'hello'   => 'Benvenuto!',
);

$translations[$groups['BTCNONNA']] = array(
  'welcome' => 'Ciao! Benvenuto nel gruppo `Bitcoin spiegati a mia nonna`! Per chiedere aiuto usa `@TutoreBot`.',
  'goodbye' => 'Arrivederci, stammi bene!',
  'hello'   => 'Ciao! Ricorda di leggere il messaggio fissato in alto. Per chiedere aiuto usa `@TutoreBot`.',
);
```

## FAQ ##
* moderatore: il moderatore e' un utente normale configurato nel bot che opera come moderatore attraverso il bot. Di conseguenza non e' necessario dare privs da admin a nessuno
